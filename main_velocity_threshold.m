%%
% Implementation of the "first approach" described in
% Saveriano et al., Merging Position and Orientation Motion Primitives,
% ICRA, 2019 (Section III.A)
%%
clear;
close all; 

addpath('myQuaternion')
addpath('poseDMP')

% Position DMP parameters
dmpPar.alphaPos = 1;
dmpPar.tauPos   = 1;
dmpPar.sigmaPos = 0.01;
dmpPar.kPos     = 10;
dmpPar.dPos     = 2*sqrt(dmpPar.kPos);
dmpPar.dtPos    = 0.01;
dmpPar.nCompPos = 15;

% Orientation DMP parameters
dmpPar.alphaQuat = 1;
dmpPar.tauQuat   = 1;
dmpPar.sigmaQuat = 0.01;
dmpPar.kQuat     = 10;
dmpPar.dQuat     = 2*sqrt(dmpPar.kQuat*dmpPar.tauQuat);
dmpPar.dtQuat    = 0.01;
dmpPar.errMethod = 'vector';
dmpPar.nCompQuat = 15;

%% Prepare training data using minimum jerk trajectory
% Goal and initial position
dmpPar1 = dmpPar;
dmpPar1.goalPos = [0; 1; -0.3];
dmpPar1.initPos = [1.5; -1; 0.8];

% Goal and initial quaterion
dmpPar1.goalQuat = quatNormalize([0.2471; 0.1797; 0.3182; -0.8974]);
dmpPar1.initQuat = quatNormalize([0.3717; -0.4993; -0.6162; 0.4825]);

initState.pos  = dmpPar1.initPos;
initState.linVel = [0;0;0];
initState.linAcc = [0;0;0];

initState.quat = dmpPar1.initQuat;
initState.angVel = [0;0;0];
initState.angAcc = [0;0;0];

goalState = initState;
goalState.pos  = dmpPar1.goalPos;
goalState.quat = dmpPar1.goalQuat;

T = 5; % Time duration

% Generate position data with minimum jerk trajectory
trainData1 = minJerkTrajectory(initState, goalState, T, dmpPar.dtPos);
dmpPar1.nbData = size(trainData1.pos, 2);

% Goal and initial position
dmpPar2 = dmpPar;
dmpPar2.initPos = [0; 1; -0.3];
dmpPar2.goalPos = [1.5; -1; 0.8];

% Goal and initial quaterion
dmpPar2.initQuat = quatNormalize([0.2471; 0.1797; 0.3182; -0.8974]);
dmpPar2.goalQuat = quatNormalize([0.3717; -0.4993; -0.6162; 0.4825]);

initState.pos  = dmpPar2.initPos;
initState.linVel = [0;0.1;0];
initState.linAcc = [0;0;0];

initState.quat = dmpPar2.initQuat;
initState.angVel = [0;0;0];
initState.angAcc = [0;0;0];

goalState = initState;
goalState.pos  = dmpPar2.goalPos;
goalState.quat = dmpPar2.goalQuat;

% Generate position data with minimum jerk trajectory
trainData2 = minJerkTrajectory(initState, goalState, T, dmpPar.dtPos);
dmpPar2.nbData = size(trainData2.pos, 2);

%% Train the DMPs separately
dmpPar1 = trainPoseDMP(trainData1, dmpPar1);
dmpPar2 = trainPoseDMP(trainData2, dmpPar2);

%% Reproduce learned trajectories
dmpPar1.initLinVel = [0;0;0];
dmpPar1.initAngVel = [0;0;0];
dmpState1 = simulatePoseDMP(dmpPar1, 0, 5000, 0.01, 0.01);

dmpPar2.initPos = dmpState1.pos(:,end);
dmpPar2.initQuat = dmpState1.quat(:,end);
dmpPar2.initLinVel = [0;0;0];

dmpPar2.initAngVel = dmpState1.angVel(:,end);
dmpState2 = simulatePoseDMP(dmpPar2, 0, 600, 1e-4, 1e-4);

%% Calculate reproduction error
% DMP 1
for i=1:size(dmpState1.pos, 2)
    if(i<=dmpPar1.nbData)
        pErr1(i) = norm(dmpState1.pos(:,i)-trainData1.pos(:,i));
        qErr1(i) = norm(quatError(dmpState1.quat(:,i), trainData1.quat(:,i), 'vector'));
    else
        pErr1(i) = norm(dmpState1.pos(:,i)-trainData1.pos(:,end));
        qErr1(i) = norm(quatError(dmpState1.quat(:,i), trainData1.quat(:,end), 'vector'));
    end
end
% DMP 2
for i=1:size(dmpState2.pos, 2)
    if(i<=dmpPar2.nbData)
        pErr2(i) = norm(dmpState2.pos(:,i)-trainData2.pos(:,i));
        qErr2(i) = norm(quatError(dmpState2.quat(:,i), trainData2.quat(:,i), 'vector'));
    else
        pErr2(i) = norm(dmpState2.pos(:,i)-trainData2.pos(:,end));
        qErr2(i) = norm(quatError(dmpState2.quat(:,i), trainData2.quat(:,end), 'vector'));
    end
end
pErr = [pErr1, pErr2];
qErr = [qErr1, qErr2];

%% Plots
currPos = [dmpState1.pos, dmpState2.pos(:, 2:end)];
currVel = [dmpState1.linVel, dmpState2.linVel(:, 2:end)];
currAcc = [dmpState1.linAcc, dmpState2.linAcc(:, 2:end)];
currQuat = [dmpState1.quat, dmpState2.quat(:, 2:end)];
currAngVel = [dmpState1.angVel, dmpState2.angVel(:, 2:end)];
trainData.pos = [trainData1.pos, trainData2.pos];
trainData.quat = [trainData1.quat, trainData2.quat];
trainData.linVel = [trainData1.linVel, trainData2.linVel];
trainData.linAcc = [trainData1.linAcc, trainData2.linAcc];
trainData.angVel = [trainData1.angVel, trainData2.angVel];
dtPos = dmpPar1.dtPos;
% Velocity trajectory merged
subplot(2,3,2)
plot(0:dtPos:dtPos*(size(currVel,2)-1), currVel, 'LineWidth', 2);
hold on;
plot(0:dtPos:dtPos*(size(trainData.linVel,2)-1), trainData.linVel, 'k');
axis([0,10,-0.8,0.8]);
title('Linear velocty')
% Position trajectory merged
subplot(2,3,1)
plot(0:dtPos:dtPos*(size(currPos,2)-1), currPos, 'LineWidth', 2, 'DisplayName', 'Position trajectory');
hold on;
axis([0,10,-1.1,1.6]);
title('Position')
plot(0:dtPos:dtPos*(size(trainData.pos,2)-1), trainData.pos, 'k');
% Orientation trajectory merged
subplot(2,3,4)
plot(0:dtPos:dtPos*(size(currQuat,2)-1), currQuat, 'LineWidth', 2, 'DisplayName', 'Orientation trajectory');
hold on;
plot(0:dtPos:dtPos*(size(trainData.quat,2)-1), trainData.quat, 'k', 'DisplayName', 'Training Data');
axis([0,10,-1,0.8]);
title('Orientation')
% Angular velocity trajectory merged
subplot(2,3,5) 
plot(0:dtPos:dtPos*(size(currAngVel,2)-1), currAngVel, 'LineWidth', 2);
hold on
plot(0:dtPos:dtPos*(size(trainData.angVel,2)-1), trainData.angVel, 'k');
axis([0,10,-2.5,2.5]);
title('Angular velocty')

% Errors
subplot(2,3,3)
plot(0:dtPos:dtPos*(length(pErr)-1),pErr,'k')
%axis([0,10,0,0.01]);
title('Position error')

subplot(2,3,6)
plot(0:dtPos:dtPos*(length(qErr)-1),qErr,'k')
%axis([0,10,0,0.014]);
title('Orientation error')
