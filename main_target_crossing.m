%%
% Implementation of the "second approach" described in
% Saveriano et al., Merging Position and Orientation Motion Primitives,
% ICRA, 2019 (Section III.B)
%%
clear;
close all; 

addpath('myQuaternion')
addpath('poseDMP')

% Position DMP parameters
dmpPar1.alphaPos = 1;
dmpPar1.tauPos   = 1;
dmpPar1.kPos     = 10;
dmpPar1.dPos     = 2*sqrt(dmpPar1.kPos*dmpPar1.tauPos);
dmpPar1.dtPos    = 0.01;
dmpPar1.nCompPos = 15;
dmpPar1.initVel = [0;0;0];
dmpPar1.goalVel = [0;0;0];

% Orientation DMP parameters
dmpPar1.alphaQuat = 1; 
dmpPar1.tauQuat   = 1;
dmpPar1.kQuat     = 10;
dmpPar1.dQuat     = 2*sqrt(dmpPar1.kQuat*dmpPar1.tauQuat);
dmpPar1.dtQuat    = 0.01;
dmpPar1.errMethod = 'vector';
dmpPar1.nCompQuat = 15;
dmpPar1.initQuatVel = [0;0;0;0];
dmpPar1.goalQuatVel = [0;0;0;0];

dmpPar2 = dmpPar1;

%% Prepare training data using minimum jerk trajectory
% Goal and initial position
dmpPar1.goalPos = [0; 1; -0.3];
dmpPar1.initPos = [1.5; -1; 0.8];

% Goal and initial quaterion
dmpPar1.goalQuat = quatNormalize([0.2471; 0.1797; 0.3182; -0.8974]);
dmpPar1.initQuat = quatNormalize([0.3717; -0.4993; -0.6162; 0.4825]);

initState.pos  = dmpPar1.initPos;
initState.linVel = [0;0;0];
initState.linAcc = [0;0;0];

initState.quat = dmpPar1.initQuat;
initState.angVel = [0;0;0];
initState.angAcc = [0;0;0];

goalState = initState;
goalState.pos  = dmpPar1.goalPos;
goalState.quat = dmpPar1.goalQuat;

%% Generate data with minimum jerk trajectory
T = 5; % Time duration
trainData1 = minJerkTrajectory(initState, goalState, T, dmpPar1.dtPos);
dmpPar1.nbData = size(trainData1.pos, 2);

dmpPar1.goalVel    = trainData1.linVel(:, end);
trainData1.angVel(:, end) = [0;0;0];
dmpPar1.goalangVel = [0;0;0];%trainData1.angVel(:, end);

dmpPar1.initangVel = trainData1.angVel(:, 1);

% DMP 2
dmpPar2.goalPos = dmpPar1.initPos;
dmpPar2.initPos = dmpPar1.goalPos;

% Goal and initial quaterion
dmpPar2.goalQuat = dmpPar1.initQuat;
dmpPar2.initQuat = dmpPar1.goalQuat;

initState.pos  = dmpPar2.initPos;
initState.quat = dmpPar2.initQuat;

goalState.pos  = dmpPar2.goalPos;
goalState.quat = dmpPar2.goalQuat;

trainData2 = minJerkTrajectory(initState, goalState, T, dmpPar2.dtPos);
dmpPar2.nbData = size(trainData2.pos, 2);

dmpPar2.goalVel    = [0;0;0];
dmpPar2.goalangVel = [0;0;0];

dmpPar2.initangVel = trainData2.angVel(:, 1);

%% Learn the DMPs
% DMP 1
dmpPar1 = trainPoseDMPblending2(trainData1, dmpPar1);

% DMP 2
dmpPar2 = trainPoseDMPblending2(trainData2, dmpPar2);

%% Simulate Pose DMP 1
dmpPar1.goalVel    = 0.01*ones(3,1); 
dmpPar1.goalangVel = 0.01*ones(3,1);

dmpState1 = simulatePoseDMPblending2(dmpPar1, 0, 501);

dmpPar2.initPos    = dmpState1.pos(:, end);
dmpPar2.initVel    = dmpState1.linVel(:, end);
dmpPar2.initQuat   = dmpState1.quat(:, end);
dmpPar2.initangVel = dmpState1.angVel(:, end);
 
%% Simulate Pose DMP
dmpState2 = simulatePoseDMPblending2(dmpPar2, 0, 1000);

%% Calculate reproduction error
% DMP 1
for i=1:size(dmpState1.pos, 2)
    if(i<=dmpPar1.nbData)
        pErr1(i) = norm(dmpState1.pos(:,i)-trainData1.pos(:,i));
        qErr1(i) = norm(quatError(dmpState1.quat(:,i), trainData1.quat(:,i), 'logarithm'));
    else
        pErr1(i) = norm(dmpState1.pos(:,i)-trainData1.pos(:,end));
        qErr1(i) = norm(quatError(dmpState1.quat(:,i), trainData1.quat(:,end), 'logarithm'));
    end
end
% DMP 2
for i=1:size(dmpState2.pos, 2)
    if(i<=dmpPar2.nbData)
        pErr2(i) = norm(dmpState2.pos(:,i)-trainData2.pos(:,i));
        qErr2(i) = norm(quatError(dmpState2.quat(:,i), trainData2.quat(:,i), 'logarithm'));
    else
        pErr2(i) = norm(dmpState2.pos(:,i)-trainData2.pos(:,end));
        qErr2(i) = norm(quatError(dmpState2.quat(:,i), trainData2.quat(:,end), 'logarithm'));
    end
end
pErr = [pErr1, pErr2];
qErr = [qErr1, qErr2];

%% Plots
plotMode = 'merging';
if strcmp('merging', plotMode)    
    currPos = [dmpState1.pos, dmpState2.pos(:, 2:end)];
    currVel = [dmpState1.linVel, dmpState2.linVel(:, 2:end)];
    currQuat = [dmpState1.quat, dmpState2.quat(:, 2:end)];
    currAngVel = [dmpState1.angVel, dmpState2.angVel(:, 2:end)];
    trainData.pos = [trainData1.pos, trainData2.pos];
    trainData.quat = [trainData1.quat, trainData2.quat];
    trainData.linVel = [trainData1.linVel, trainData2.linVel];
    trainData.angVel = [trainData1.angVel, trainData2.angVel];
    dtPos = dmpPar1.dtPos;
    % Velocity trajectory merged
    %figure('NumberTitle', 'off', 'Name', 'Velocity trajectory merged'); 
    subplot(2,4,2)
    plot(0:dtPos:dtPos*(size(currVel,2)-1), currVel, 'LineWidth', 2);
    hold on;
    plot(0:dtPos:dtPos*(size(trainData.linVel,2)-1), trainData.linVel, 'k');
    axis([0,10,-0.8,0.8]);
    title('Linear velocty')
    % Position trajectory merged
    subplot(2,4,1)
    plot(0:dtPos:dtPos*(size(currPos,2)-1), currPos, 'LineWidth', 2, 'DisplayName', 'Position trajectory');
    hold on;
    axis([0,10,-1.1,1.6]);
    title('Position')
    plot(0:dtPos:dtPos*(size(trainData.pos,2)-1), trainData.pos, 'k', 'LineWidth', 2);
    % Moving target
    subplot(2,4,4)
    plot(0:dtPos:dtPos*(size(dmpState1.tmPos,2)-1), dmpState1.tmPos, 'LineWidth', 2);
    hold on
    plot([dtPos*(size(dmpState1.tmPos,2)) dtPos+dtPos*(size(dmpState1.tmPos,2))], [dmpState1.tmPos(:,end) dmpState2.tmPos(:,1)], 'LineWidth', 2);
    plot(dtPos*(size(dmpState1.tmPos,2)):dtPos:dtPos*(2*size(dmpState2.tmPos,2)+1), dmpState2.tmPos, 'LineWidth', 2);
    axis([0,10,-1.1,1.6])
    title('Moving target pos')

    % Orientation trajectory merged
    subplot(2,4,5)
    plot(0:dtPos:dtPos*(size(currQuat,2)-1), currQuat, 'LineWidth', 2, 'DisplayName', 'Orientation trajectory');
    hold on;
    plot(0:dtPos:dtPos*(size(trainData.quat,2)-1), trainData.quat, 'k', 'DisplayName', 'Training Data');
    axis([0,10,-1,0.8]);
    title('Orientation')
    % Angular velocity trajectory merged
    subplot(2,4,6) 
    plot(0:dtPos:dtPos*(size(currAngVel,2)-1), currAngVel, 'LineWidth', 2);
    hold on;
    plot(0:dtPos:dtPos*(size(trainData.angVel,2)-1), trainData.angVel, 'k');
    axis([0,10,-2.5,2.5]);
    title('Angular velocty')

    % Errors
    subplot(2,4,3)
    plot(0:dtPos:dtPos*(length(pErr)-1),pErr,'k')
    title('Position error')

    subplot(2,4,7)
    plot(0:dtPos:dtPos*(length(qErr)-1),qErr,'k')
    title('Orientation error')
    
    subplot(2,4,8)
    plot(0:dtPos:dtPos*(size(dmpState1.tmQuat,2)-1), dmpState1.tmQuat, 'LineWidth', 2);
    hold on
    plot([dtPos*(size(dmpState1.tmQuat,2)) dtPos+dtPos*(size(dmpState1.tmQuat,2))], [dmpState1.tmQuat(:,end) dmpState2.tmQuat(:,1)], 'LineWidth', 2);
    plot(dtPos*(size(dmpState1.tmQuat,2)):dtPos:dtPos*(2*size(dmpState2.tmQuat,2)+1), dmpState2.tmQuat, 'LineWidth', 2);
    axis([0,10,-1,0.6])
    title('Moving target quat')
end