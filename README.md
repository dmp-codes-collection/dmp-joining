# dmp-joining

An implementation of the three DMP joining approaches described in [(Saveriano et al., 2019)](https://doi.org/10.1109/ICRA.2019.8793786) and in [(Saveriano et al., 2021)](https://arxiv.org/abs/2102.03861).

## Demos description
- `main_velocity_threshold.m`: a demo to reproduce the results of the _velocity threshold_ approach described in [(Saveriano et al., 2021)](https://arxiv.org/abs/2102.03861) (Section 3.3).
- `main_target_crossing.m`: a demo to reproduce the results of the _target crossing_ approach described in [(Saveriano et al., 2021)](https://arxiv.org/abs/2102.03861) (Section 3.3).
- `main_basis_functions_overlay.m`: a demo to reproduce the results of the _basis functions overlay_ approach described in [(Saveriano et al., 2021)](https://arxiv.org/abs/2102.03861) (Section 3.3).

## Software requirements
The code is developed and tested under `Ubuntu 18.04` and `Matlab2015b`.

## References
Please acknowledge the authors in any academic publication that used parts of these codes.
```
@inproceedings{saveriano2019merging,
	author = {Saveriano, M. and Franzel, F. and Lee, D.},
	booktitle = {IEEE International Conference on Robotics and Automation},
	title = {Merging position and orientation motion primitives},
	pages={7041--7047},
	address={Montreal, QC, Canada},
	year = {2019}
}

@article{saveriano2021dynamic,
  author = {Saveriano, M. and Abu-Dakka, F. J. and Kramberger, A. and  Peternel, L.},
  title = {Dynamic Movement Primitives in Robotics: A Tutorial Survey},
  journal={arXiv preprint arXiv:2102.03861},
  year={2021}
}
```

## Note
This source code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.
