% Returns the angular velocity given the quaternion rate \dot{q}
function w = quatRateToAngularVelocity(q, qDot)
    if(size(q,1)~=4 || size(qDot,1)~=4)
        error('quatRateToAngularVelocity(q, qDot): quaternion must be 4x1');
    end
    
    qVel = 2*quatProduct(qDot, quatConjugate(q));
    
    w = qVel(2:4);
end