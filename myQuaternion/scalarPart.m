% Returns the scalar part of a quaternion q = [\eta, \epsilon^T]^T
function eta = scalarPart(q)
    if(size(q,1)~=4)
        error('scalarPart(q): quaternion must be 4x1');
    end
    
    eta = q(1);
end