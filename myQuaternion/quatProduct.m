% Returns the product of two quaternions q = q1*q2
function qProduct = quatProduct(q1, q2)
    if(size(q1,1)~=4 || size(q2,1)~=4)
        error('quatProduct(q1, q2): quaternions must be 4x1');
    end
    
    qProduct = zeros(4,1);
    
    qProduct(1)   = q1(1)*q2(1) - q1(2:4)'*q2(2:4);
    qProduct(2:4) = q1(1)*q2(2:4) + q2(1)*q1(2:4) + cross(q1(2:4),q2(2:4));
end