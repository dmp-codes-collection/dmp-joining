% Returns the exponential map R^3 -> S^3 of a quaternion
function qExp = quatExponential(r)
    if(size(r,1)~=3)
        error('quatExponential(r): rotation vector must be 3x1');
    end
    
    qExp = zeros(4,1);
    
    nR = norm(r);
    
    if(nR > pi)
        error('quatExponential(r): ||r|| > pi');
    end
    
    if(nR > 1e-8)
        qExp(1)   = cos(nR);
        qExp(2:4) = sin(nR)*r./nR;
    else
        qExp(1) = 1;
    end
end