% Returns the norm of a quaternion ||q|| = sqrt(\eta^2 + \epsilon^T\epsilon)
function qNorm = quatNorm(q)
    if(size(q,1)~=4)
        error('quatNorm(q): quaternion must be 4x1');
    end
    
    %qNorm = abs(q(1)) + norm(q(2:4));  
    qNorm = norm(q);
end