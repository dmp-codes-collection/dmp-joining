% Returns a skew-symmetric matrix given a vector w
function R = skewSymmetric(w)
    if(size(w,1)~=3)
        error('skewSymmetric(w): vector must be 3x1');
    end

    R = zeros(3,3);

    R(1,2) = -w(3); R(1,3) =  w(2); R(2,3) = -w(1);
    R(2,1) =  w(3); R(3,1) = -w(2); R(3,2) =  w(1);  
end