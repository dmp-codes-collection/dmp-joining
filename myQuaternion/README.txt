A library of functions that implements Unit Quaternion algebra

Author: Matteo Saveriano

Note: I represent quaternions as 4-dimensional column vectors q where q(1) is the scalar part \eta and q(2:4) is the vector part \epsilon 
