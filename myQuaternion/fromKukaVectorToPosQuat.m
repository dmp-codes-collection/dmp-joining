function [ p, q ] = fromKukaVectorToPosQuat( rotMatrix )
    p = zeros(3, size(rotMatrix, 1));
    q = zeros(4, size(rotMatrix, 1));

    qPrev   = [1,0,0,0]';
    maximum = 1;
    signum  = 1;

    for i = 1:size(rotMatrix, 1)
        p(:, i) = [rotMatrix(i, 4), rotMatrix(i, 8), rotMatrix(i, 12)];
        R = [rotMatrix(i, 1:3); rotMatrix(i, 5:7); rotMatrix(i, 9:11)];
        [q(:, i), maximum, signum] = rotToQuat(R, qPrev, maximum, signum);
        qPrev = q(:, i); 
    end
end

