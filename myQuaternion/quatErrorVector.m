% Returns the error in R^3 between two quaternions using the vector part
function qErr = quatErrorVector(q1, q2)
    if(size(q1,1)~=4 || size(q2,1)~=4)
        error('quatErrorVector(q1, q2): quaternions must be 4x1');
    end
    
    qErr = 2*vectorPart(quatProduct(q1, quatConjugate(q2)));
end