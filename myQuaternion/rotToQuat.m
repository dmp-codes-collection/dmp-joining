function [q,maximum,signum] = rotToQuat(S, q_n1,maximum_n1,signum_n1)
% This block supports the Embedded MATLAB subset.
% See the help menu for details. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% Quaternions out of rotation matrix %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % calculation depends on the greatest element  
% % on the diagonal of the rotation Matrix
tr = [1+S(1,1)+S(2,2)+S(3,3)
      1+S(1,1)-S(2,2)-S(3,3)
      1-S(1,1)+S(2,2)-S(3,3)
      1-S(1,1)-S(2,2)+S(3,3)];
[~, maximum] = max(tr);

% calculate the quaternions
if (maximum == 1)      % q0 is the greatest
    r = 2*sqrt(tr(1));
    q = [r/4
         (S(3,2)-S(2,3))/r
         (S(1,3)-S(3,1))/r
         (S(2,1)-S(1,2))/r];
elseif (maximum == 2)  % q1 is the greatest
   r = 2*sqrt(tr(2));
   q = [(S(3,2)-S(2,3))/r
        r/4
        (S(2,1)+S(1,2))/r
        (S(1,3)+S(3,1))/r];
elseif (maximum == 3)  % q2 ist the greatest
   r = 2*sqrt(tr(3));
   q = [(S(1,3)-S(3,1))/r
        (S(2,1)+S(1,2))/r
        r/4
        (S(3,2)+S(2,3))/r];
else                   % q3 is the greatest
   r = 2*sqrt(tr(4));
   q = [(S(2,1)-S(1,2))/r
        (S(1,3)+S(3,1))/r
        (S(3,2)+S(2,3))/r
        r/4];       
end

%The sign of the quaternions can be wrong -> there will be a step in q0
signum = signum_n1;
if (maximum_n1 ~= maximum)
    if (norm(signum*q(1)-q_n1(1)) > 0.1)
            signum = -signum;
    end
end
q = signum*q;