% Returns the conjugate of a quaternion \bar{q} = [\eta, -\epsilon^T]^T
function qConjugate = quatConjugate(q)
    if(size(q,1)~=4)
        error('quatConjugate(q): quaternion must be 4x1');
    end
    
    qConjugate = q;
    qConjugate(2:4) = -qConjugate(2:4);
end