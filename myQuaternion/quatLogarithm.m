% Returns the logarithmic map S^3 -> R^3 of a quaternion
function qLog = quatLogarithm(q)
    if(size(q,1)~=4)
        error('quatLogarithm(q): quaternion must be 4x1');
    end
    
    qLog = zeros(3,1);
    
    nEpsilon = norm(q(2:4));
    
    if(nEpsilon>1e-6)
        qLog = acos(q(1))*q(2:4)./nEpsilon;
    end
end