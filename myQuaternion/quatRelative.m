% Returns the relative orientation (quaternion) between two quaternions
function qRel = quatRelative(q1, q2)
    if(size(q1,1)~=4 || size(q2,1)~=4)
        error('quatRelative(q1, q2): quaternions must be 4x1');
    end
    
    qRel = quatNormalize(quatProduct(q1, quatConjugate(q2)));
end