% Returns a unit quaternion with unitary norm ||q|| = 1
function qUnit = quatNormalize(q)
    if(size(q,1)~=4)
        error('quatNormalize(q): quaternion must be 4x1');
    end
    
    qUnit = q./norm(q);    
end
