% Returns the error in R^3 between two quaternions using user defined
% method
function qErr = quatError(q1, q2, method)
    if(size(q1,1)~=4 || size(q2,1)~=4)
        error('quatError(q1, q2): quaternions must be 4x1');
    end
    
    if(strcmp(method, 'vector'))
        qErr = quatErrorVector(q1, q2);
    elseif(strcmp(method, 'logarithm'))
        qErr = quatErrorLogarithm(q1, q2);
    end
end