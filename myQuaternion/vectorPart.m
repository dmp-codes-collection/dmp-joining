% Returns the vector part of a quaternion q = [\eta, \epsilon^T]^T
function epsilon = vectorPart(q)
    if(size(q,1)~=4)
        error('vectorPart(q): quaternion must be 4x1');
    end
    
    epsilon = q(2:4);
end