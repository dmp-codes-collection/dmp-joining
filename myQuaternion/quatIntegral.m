% Returns q(t+dt) by numerically integrating the angular velocity 
function qNext = quatIntegral(q, w, dt)
    if(size(q,1)~=4)
        error('quatIntegral(q, w, dt): quaternion must be 4x1');
    end
    if(size(w,1)~=3)
        error('quatIntegral(q, w, dt): angular velocity must be 3x1');
    end
    
    qNext = quatProduct(quatExponential(dt*w./2), q);
    % qNext = q + quatPropagation(q, w)*dt; % Same as previous
end
