function [ KukaMatrix ] = fromPosQuatToKukaVector( p, q )

KukaMatrix = zeros(size(p, 2), 12);

for i = 1:size(p, 2)
    R = quatToRot(q(:, i));
    KukaMatrix(i, :) = [R(1, :), p(1, i), R(2, :), p(2, i), R(3, :), p(3, i)];
end


end

