% Returns the time derivatives of a quaternion \dot{q} = 0.5 w*q
function qDot = quatPropagation(q, w)
    if(size(q,1)~=4)
        error('quatPropagation(q, w): quaternion must be 4x1');
    end
    if(size(w,1)~=3)
        error('quatPropagation(q, w): angular velocity must be 3x1');
    end
    
    qDot = 0.5*quatProduct([0; w], q);
end