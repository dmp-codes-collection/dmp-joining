% Check if each element  i of the cell array v{} is a len{i}x1 vector
% varargin to specify an error message 'errorMsg'
function res = checkInputSize(v, len, varargin)
    inNum = length(v);
    res   = 0;
    
    if(~isempty(varargin)>0)
        if(strcmp(varargin{1}, 'errorMsg'))
            errorMsg = varargin{2};
        end
    else
        errorMsg = {'',''};
    end
    
    for i=1:inNum
        [row, col] = size(v{i});
    
        if(row~=len{i} || col~=1)
            error([errorMsg{i} ' vector must me of size ' num2str(len{i}) 'x1']);
            break;
        else
            res = 1;
        end
    end
end