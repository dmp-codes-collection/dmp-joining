% Fit pose DMP using Weigthed Least Square (WLS)
% Uses quaternion representation
function dmpPar = trainThirdApproachDMP(trainData, initPar)
    dmpPar = initPar;
    
    %% Learning position forcing term 
    % Position DMP parameters
    
    alphaPos  = initPar.alphaPos;
    tauPos    = initPar.tauPos;
    sigmaPos  = initPar.sigmaPos;
    kPos      = initPar.kPos;
    dPos      = initPar.dPos;
    goalPos   = initPar.goalPos;
    initPos   = initPar.initPos;
    nCompPos  = initPar.nCompPos;  % Number of RBF for the position
    dtPos     = initPar.dtPos;
    
    nDataPos = size(trainData.pos, 2); %Length of position trajectory

    % Centers equally distributed
%     MuD    = linspace(nDataPos, 1, nCompPos);
%     SigmaD = 10*nDataPos;
    
    % Estimate Mu_s and Sigma_s to match Mu_d and Sigma_d
    %MuClockPos(1,:) = exp(-alphaPos*MuD*dtPos);
    MuClockPos = linspace(0, 1, nCompPos);
    SigmaClockPos = (2*sigmaPos^2*ones(1, nCompPos));
%     SigmaClockPos = zeros(1, nCompPos);
%     for i=1:nCompPos
%         std_s = MuClockPos(i) - exp(-alphaPos*(MuD(i)+SigmaD^.5)*dtPos);
%         SigmaClockPos(i) = std_s^2;
%     end
    
    % WLS learning: least norm solution to find MuPos (Y = MuPos*H')
    sPos = 1; % Decay term
    fPos = zeros(3, nDataPos);
    H = zeros(nDataPos, nCompPos);
    goalFunc = initPos;
    for n=1:nDataPos
        %sPos = sPos - alphaPos*sPos*dtPos;
        sPos = 1/(1+exp(-alphaPos*(nDataPos*tauPos-n)));
        h = zeros(1,nCompPos);
        for i=1:nCompPos
            %h(i) = gaussPDF(sPos, MuClockPos(i), SigmaClockPos(i));
            h(i) = gaussPDF((n-1)/nDataPos*tauPos, MuClockPos(i), SigmaClockPos(i));
        end
        % Compute weights
        H(n,:) = h./sum(h);
        % Compute force by inverting DMP equation
        fPosA = kPos\(tauPos^2*trainData.linAcc(:,n) + dPos*tauPos*trainData.linVel(:,n));
        fPosB = -(goalFunc-trainData.pos(:,n))+ 0*(goalPos-trainData.pos(:,1))*sPos;
        fPos(:,n) = (fPosA + fPosB)./sPos;
        goalFunc = tauPos*n*(goalPos-initPos)/nDataPos + initPos;
    end
      
    % Compute least square solution (inv(H'*H)*H'*Y')'
    dmpPar.posForceW = (pinv(H)*fPos')';
    dmpPar.posCenter = MuClockPos;
    dmpPar.posAmp    = SigmaClockPos;
    
    %% Learning orientation forcing term
    % Orientation DMP parameters
    alphaQuat = initPar.alphaQuat;
    tauQuat   = initPar.tauQuat;
    sigmaQuat = initPar.sigmaQuat;
    kQuat     = initPar.kQuat;
    dQuat     = initPar.dQuat;
    goalQuat  = initPar.goalQuat;
    initQuat  = initPar.initQuat;
    nCompQuat = initPar.nCompQuat; % Number of RBF for the quaternion
    dtQuat    = initPar.dtQuat;
    
    nDataQuat = size(trainData.quat, 2); % Length of orientation trajectory
    
    % Centers equally distributed
    MuClockQuat = linspace(0, 1, nCompQuat);
    SigmaClockQuat = (2*sigmaQuat^2*ones(1, nCompQuat));
    
%     MuD    = linspace(nDataQuat, 1, nCompQuat);
%     SigmaD = 10*nDataQuat;
%     
%     % Estimate Mu_s and Sigma_s to match Mu_d and Sigma_d
%     MuClockQuat(1,:) = exp(-alphaQuat*MuD*dtQuat);
%     SigmaClockQuat = zeros(1, nCompQuat);
%     for i=1:nCompQuat
%         std_s = MuClockQuat(i) - exp(-alphaQuat*(MuD(i)+SigmaD^.5)*dtQuat);
%         SigmaClockQuat(i) = std_s^2;
%     end
    
    % WLS learning: least norm solution to find MuQuat (Y = MuQuat*H')
    sQuat = 1; % Decay term
    fQuat = zeros(3, nDataQuat);
    goalQuatFunc = initQuat;
    H = zeros(nDataQuat, nCompQuat);
    Func = zeros(4,nCompQuat);
    quatAngVel = quatError(goalQuat, initQuat, 'logarithm')/(dmpPar.nbData);
    for n=1:nDataQuat
        Func(:, n) =  goalQuatFunc;
        %sQuat = sQuat - alphaQuat*sQuat*dtQuat;
        sQuat = 1/(1+exp(-alphaQuat*(nDataQuat*tauQuat-n)));
        h = zeros(1,nCompQuat);
        for i=1:nCompQuat
            %h(i) = gaussPDF(sQuat, MuClockQuat(i), SigmaClockQuat(i));
            h(i) = gaussPDF((n-1)/nDataPos*tauQuat, MuClockQuat(i), SigmaClockQuat(i));
        end
        % Compute weights
        H(n,:) = h./sum(h);
        % Compute force by inverting DMP equation
        fQuatA = kQuat\(tauQuat^2*trainData.angAcc(:,n) + dQuat*tauQuat*trainData.angVel(:,n));
        fQuatB = -quatError(goalQuatFunc, trainData.quat(:,n), dmpPar.errMethod);
        fQuatC =  0*quatError(goalQuat, trainData.quat(:,1), dmpPar.errMethod)*sQuat;
        fQuat(:,n) = (fQuatA + fQuatB + fQuatC)./sQuat;
        % Compute goal function
       
        goalQuatFunc = quatIntegral(goalQuatFunc, tauQuat*quatAngVel, 1);
        goalQuatFunc = quatNormalize(goalQuatFunc);
    end
%     plot(Func(1, :));
%     hold on;
%     plot(Func(2, :));
%     plot(Func(3, :));
%     plot(dmpPar.nbData, goalQuat, 'x');
%     plot(1, initQuat, 'x');
    % Compute least square solution (inv(H'*H)*H'*Y')'
    dmpPar.quatForceW = (pinv(H)*fQuat')';
    dmpPar.quatCenter = MuClockQuat;
    dmpPar.quatAmp    = SigmaClockQuat;
end
