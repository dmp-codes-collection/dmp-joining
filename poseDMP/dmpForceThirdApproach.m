% Returns DMP nonlinear forcing term
function f = dmpForceThirdApproach(forceW, center, amplitude, K, clockSignal, itNum, nbData)
    if nargin < 7
        timeScale = clockSignal;
    else
        timeScale = itNum / nbData;
    end
    % Compute activation functions
    nComponents = size(center, 2);
    psi = zeros(nComponents, 1);
    for i = 1:nComponents
        psi(i,1) = gaussPDF(timeScale, center(i), amplitude(i));
    end
    psi = psi./sum(psi);
    
    % Compute forcing term
    f = K*forceW*psi*clockSignal;
end
