% Returns DMP nonlinear forcing term
function f = dmpNonlinearForce(forceW, center, amplitude, K, clockSignal)
    % Compute activation functions
    nComponents = size(center, 2);
    psi = zeros(nComponents, 1);
    for i = 1:nComponents
        psi(i,1) = gaussPDF(clockSignal, center(i), amplitude(i));
    end
    psi = psi./sum(psi);
    
    % Compute forcing term
    f = K*forceW*psi*clockSignal;
end
