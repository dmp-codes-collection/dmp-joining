% Compute a minimum jerk trajectory from init pose to goal pose using
% unit quaternions to represent the orientation

function stateTraj = minJerkTrajectory(initState, goalState, totTime, dt, varargin)
    fullPose  = 1;
    if(~isempty(varargin)>0)
        if(strcmp(varargin{1},'fullPose'))
            fullPose = varargin{2};
        end
    end
    
    stateTraj = initState;
   
    x   = stateTraj.pos;
    xd  = stateTraj.linVel;
    xdd = stateTraj.linAcc;
    q   = stateTraj.quat;
    qd  = [0; 0; 0; 0];
    qdd = [0; 0; 0; 0];
    itNum = 1;
    while(totTime > dt)
        itNum = itNum + 1;
        
        % Position
        for i=1:3
            [x(i), xd(i), xdd(i)] = minJerkStep(x(i), xd(i), xdd(i), goalState.pos(i), totTime, dt);
        end
        
        stateTraj.pos(:, itNum)    = x;
        stateTraj.linVel(:, itNum) = xd;
        stateTraj.linAcc(:, itNum) = xdd;
        
        % Orientation 
        for i=1:4
            [q(i), qd(i), qdd(i)] = minJerkStep(q(i), qd(i), qdd(i), goalState.quat(i), totTime, dt);
        end
        
        stateTraj.quat(:, itNum)  = quatNormalize(q(:));
%         currState.angVel = qd;
%         currState.angAcc = qdd;

        totTime = totTime - dt;
    end
    
    % Compute angular velocity from quaternion rate
    nbData = size(stateTraj.quat, 2);
    qd(:, 1) = [0; 0; 0; 0];
    qd(:, 2:nbData) = (stateTraj.quat(:, 2:end) - stateTraj.quat(:, 1:end-1))./dt;
%     qd(:, nbData) = [0; 0; 0; 0];
    for i=1:nbData
        stateTraj.angVel(:,i) = quatRateToAngularVelocity(stateTraj.quat(:,i), qd(:,i));
    end
    
    stateTraj.angAcc = (stateTraj.angVel(:,2:end) - stateTraj.angVel(:,1:end-1))./dt;
    stateTraj.angAcc(:, nbData) = [0; 0; 0];
end


% function [x,xd,xdd] = minJerkStep(x,xd,xdd,goal,tau, dt) computes
% the update of x,xd,xdd for the next time step dt given that we are
% currently at x,xd,xdd, and that we have tau until we want to reach
% the goal
function [x, xd, xdd] = minJerkStep(x, xd, xdd, goal, tau, dt)
    if(tau < dt)
        return;
    end

    dist = goal - x;

    a1 = 0;
    a0 = xdd * tau^2;
    v1 = 0;
    v0 = xd * tau;

    t1 = dt;
    t2 = dt^2;
    t3 = dt^3;
    t4 = dt^4;
    t5 = dt^5;

    c1 = (6.*dist + (a1 - a0)/2. - 3.*(v0 + v1))/tau^5;
    c2 = (-15.*dist + (3.*a0 - 2.*a1)/2. + 8.*v0 + 7.*v1)/tau^4;
    c3 = (10.*dist+ (a1 - 3.*a0)/2. - 6.*v0 - 4.*v1)/tau^3;
    c4 = xdd/2.;
    c5 = xd;
    c6 = x;

    x   = c1*t5 + c2*t4 + c3*t3 + c4*t2 + c5*t1 + c6;
    xd  = 5.*c1*t4 + 4*c2*t3 + 3*c3*t2 + 2*c4*t1 + c5;
    xdd = 20.*c1*t3 + 12.*c2*t2 + 6.*c3*t1 + 2.*c4;
end