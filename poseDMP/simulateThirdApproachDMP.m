function dmpState = simulateThirdApproachDMP(totaldmpPar, plotPose, maxIter)
    if(isempty(maxIter))
        maxIter = 1000;
    end
    dmpPar = totaldmpPar{1};
    dmpPar.posForceW  = [totaldmpPar{1}.posForceW,  totaldmpPar{2}.posForceW];
    %dmpPar.posCenter  = linspace(0, 1, 2*size(totaldmpPar{1}.posCenter, 2));
    dmpPar.posCenter  = [linspace(0, 0.5, totaldmpPar{1}.nCompPos) linspace(0.5, 1, totaldmpPar{2}.nCompPos)];
    dmpPar.posAmp     = [totaldmpPar{1}.posAmp,     totaldmpPar{2}.posAmp]./2;
    dmpPar.quatForceW = [totaldmpPar{1}.quatForceW, totaldmpPar{2}.quatForceW];
    %dmpPar.quatCenter  = linspace(0, 1, 2*size(totaldmpPar{1}.quatCenter, 2));
    dmpPar.quatCenter  = [linspace(0, 0.5, totaldmpPar{1}.nCompQuat) linspace(0.5, 1, totaldmpPar{2}.nCompQuat)];
    dmpPar.quatAmp    = [totaldmpPar{1}.quatAmp,    totaldmpPar{2}.quatAmp]./2;
    % Initial DMP state
    currState.pos = dmpPar.initPos;
    currState.goalFunc = dmpPar.initPos;
    currState.linVel = [0;0;0];
    currState.quat = dmpPar.initQuat;
    currState.goalQuatFunc = dmpPar.initQuat;
    currState.angVel = [0;0;0];
    currClock.pos  = 1;
    currClock.quat = 1;
    dmpState = currState;
    duration = totaldmpPar{1}.nbData + totaldmpPar{2}.nbData ;
    % Loop variables
    itNum    = 1;
    posErrNorm  = norm(dmpPar.goalPos - currState.pos);    
    quatErrNorm = norm(quatError(dmpPar.goalQuat, currState.quat, dmpPar.errMethod));

    
    while(itNum<1200) %(posErrNorm>1e-3 || quatErrNorm>1e-3) && itNum<maxIter)
            % Switch goal position
        if(itNum == totaldmpPar{1}.nbData)
            dmpPar = totaldmpPar{2};
            dmpPar.posForceW  = [totaldmpPar{1}.posForceW,  totaldmpPar{2}.posForceW];
            %dmpPar.posCenter  = linspace(0, 1, 2*size(totaldmpPar{1}.posCenter, 2));
            dmpPar.posCenter  = [linspace(0, 0.5, totaldmpPar{1}.nCompPos) linspace(0.5, 1, totaldmpPar{2}.nCompPos)];
            dmpPar.posAmp     = [totaldmpPar{1}.posAmp,     totaldmpPar{2}.posAmp]./2;
            dmpPar.quatForceW = [totaldmpPar{1}.quatForceW, totaldmpPar{2}.quatForceW];
            %dmpPar.quatCenter  = linspace(0, 1, 2*size(totaldmpPar{1}.quatCenter, 2));
            dmpPar.quatCenter  = [linspace(0, 0.5, totaldmpPar{1}.nCompQuat) linspace(0.5, 1, totaldmpPar{2}.nCompQuat)];
            dmpPar.quatAmp    = [totaldmpPar{1}.quatAmp,    totaldmpPar{2}.quatAmp]./2;
%             
%             dmpPar.initPos = currState.pos;
%             %currState.linVel = 1e-3*[-0.2; 0.3; -0.1];
        end
        
        
        % Update DMP state
        [currState, currClock] = computeNextStateThirdApproachDMP(dmpPar, currState, currClock,  itNum, duration);
        
        itNum = itNum + 1;
        
        % Store DMP state
        dmpState.pos(:,itNum) = currState.pos;
        dmpState.linVel(:,itNum) = currState.linVel;
        dmpState.quat(:,itNum) = currState.quat;
        dmpState.angVel(:,itNum) = currState.angVel;
        
        dmpState.goalQuatFunc(:,itNum) = currState.goalQuatFunc;
        dmpState.goalFunc(:,itNum) = currState.goalFunc;

        % Recalculate distance to the goal
        posErrNorm  = norm(dmpPar.goalPos - currState.pos);
        quatErrNorm = norm(quatError(dmpPar.goalQuat, currState.quat, dmpPar.errMethod));
        
%         for i = 1:dmpPar.nCompPos*2
%             psi(i,itNum) = gaussPDF(itNum/1000, dmpPar.posCenter(i), dmpPar.posAmp(i));
%         end
%         psi(:, itNum) = psi(:, itNum)./sum(psi(:, itNum));
%         force(itNum) = dmpPar.posForceW(2, :)*psi(:, itNum)*currClock.pos;
    end
    

%     for i = 1:dmpPar.nCompPos
%         plot(psi(i,:), 'k');
%         hold on;
%     end
%     for i = dmpPar.nCompPos:dmpPar.nCompPos*2
%         plot(psi(i,:), 'r');
%         hold on;
%     end
%     plot(force);
    % Plot results
    if(plotPose)
        currPos = dmpState.pos;
        dtPos = dmpPar.dtPos;
        figure('NumberTitle', 'off', 'Name', 'Position trajectory'); 
        plot(0:dtPos:dtPos*(size(currPos,2)-1), currPos, 'LineWidth', 2);
        hold on;
        plot(0:dtPos:dtPos*(size(currPos,2)-1), repmat(dmpPar.goalPos,1,size(currPos,2)))
        
        currQuat = dmpState.quat;
        dtQuat = dmpPar.dtQuat;
        figure('NumberTitle', 'off', 'Name', 'Orientation trajectory'); 
        plot(0:dtQuat:dtPos*(size(currQuat,2)-1), currQuat, 'LineWidth', 2);
        hold on;
        plot(0:dtQuat:dtPos*(size(currQuat,2)-1), repmat(dmpPar.goalQuat,1,size(currQuat,2)))
    end
end
