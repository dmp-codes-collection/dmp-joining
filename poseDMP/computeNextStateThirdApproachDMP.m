function [nextState, nextClock] = computeNextStateThirdApproachDMP(dmpPar, currState, currClock, itNum, duration, varargin)
    %% Get DMP parameters
    % Position DMP parameters
    alphaPos = dmpPar.alphaPos;
    tauPos   = dmpPar.tauPos;
    kPos     = dmpPar.kPos;
    dPos     = dmpPar.dPos;
    goalPos  = dmpPar.goalPos;
    initPos  = dmpPar.initPos;
    dtPos    = dmpPar.dtPos;
    sPos     = currClock.pos;
    goalFunc = currState.goalFunc;
    
    % Orientation DMP parameters
    alphaQuat = dmpPar.alphaQuat;
    tauQuat   = dmpPar.tauQuat;
    kQuat     = dmpPar.kQuat;
    dQuat     = dmpPar.dQuat;
    goalQuat  = dmpPar.goalQuat;
    initQuat  = dmpPar.initQuat;
    dtQuat    = dmpPar.dtQuat;
    sQuat     = currClock.quat;
    
    %% Check for user defined extra forces
    if(~isempty(varargin)>0)
        extraForce = varargin{1};
        if(length(varargin)>1)
            extraForceInt = varargin{2};
        end
    else
        extraForce.pos  = [0; 0; 0];
        extraForce.quat = [0; 0; 0];
        extraForceInt.pos  = [0; 0; 0];
        extraForceInt.quat = [0; 0; 0];
    end
    
    %% Update DMP State
    % Position
    %sPos          = sPos + (-alphaPos*sPos)*dtPos/tauPos;
    sPos = 1/(1+exp(-alphaPos*(duration*tauPos-itNum)));
    nextClock.pos = sPos;

    posErr = goalFunc - currState.pos;
    posDis = 0*(goalPos - initPos)*sPos;
    linAcc = (kPos*(posErr - posDis) - dPos*currState.linVel + extraForce.pos)/tauPos;
    goalFunc = goalFunc + tauPos*(goalPos- initPos)/dmpPar.nbData;
    if(itNum > duration-1)
        goalFunc = goalPos;
    end
    nextState.goalFunc = goalFunc;
    % Compute nonlinear forcing term
    if(isfield(dmpPar,'posForceW'))
        if(~isempty(dmpPar.posForceW))
            f = dmpForceThirdApproach(dmpPar.posForceW, dmpPar.posCenter, dmpPar.posAmp, dmpPar.kPos, sPos, itNum, duration*tauPos);
            linAcc = linAcc + f;
        end
    end
    % Update linear velocity
    nextState.linVel = currState.linVel + linAcc*dtPos + extraForceInt.pos;
    % Update position
    nextState.pos = currState.pos + nextState.linVel*dtPos; 

    % Orientation
    %sQuat          = sQuat + (-alphaQuat*sQuat)*dtQuat/tauQuat;
    sQuat = 1/(1+exp(-alphaQuat*(duration*tauPos-itNum)));
    nextClock.quat = sQuat;
    quatErr = quatError(currState.goalQuatFunc, currState.quat, dmpPar.errMethod);
    quatDis = 0*quatError(goalQuat, initQuat, dmpPar.errMethod)*sQuat;
    angAcc  = (kQuat*(quatErr - quatDis) - dQuat*currState.angVel + extraForce.quat)/tauQuat;
    % Compute goal quat function
    quatAngVel = quatError(goalQuat, initQuat, 'logarithm')/(dmpPar.nbData);
    nextState.goalQuatFunc = quatNormalize(quatIntegral(currState.goalQuatFunc, tauQuat*quatAngVel, 1));
    if(itNum >duration-1)
        nextState.goalQuatFunc = goalQuat;
    end
    % Compute nonlinear forcing term
    if(isfield(dmpPar,'quatForceW'))
        if(~isempty(dmpPar.quatForceW))
            f = dmpForceThirdApproach(dmpPar.quatForceW, dmpPar.quatCenter, dmpPar.quatAmp, dmpPar.kQuat, sQuat, itNum, duration*tauQuat);
            angAcc = angAcc + f;
        end
    end
    % Update angular velocity
    nextState.angVel = currState.angVel + angAcc*dtQuat + extraForceInt.quat;
    % Update orientation
    nextState.quat = quatNormalize(quatIntegral(currState.quat, nextState.angVel, dtQuat));
end
