function [nextState, nextClock] = computeNextStatePoseDMPblending2(dmpPar, currState, currClock, varargin)
    %% Get DMP parameters
    % Position DMP parameters
    alphaPos = dmpPar.alphaPos;
    tauPos   = dmpPar.tauPos;
    kPos     = dmpPar.kPos;
    dPos     = dmpPar.dPos;
    goalPos  = dmpPar.goalPos;
    goalVel  = dmpPar.goalVel;
    initPos  = dmpPar.initPos;
    dtPos    = dmpPar.dtPos;
    sPos     = currClock.pos;
    dataLength  = dmpPar.nbData;
    
    % Orientation DMP parameters
    alphaQuat = dmpPar.alphaQuat;
    tauQuat   = dmpPar.tauQuat;
    kQuat     = dmpPar.kQuat;
    dQuat     = dmpPar.dQuat;
    goalQuat  = dmpPar.goalQuat;
    goalangVel = dmpPar.goalangVel;
    initQuat  = dmpPar.initQuat;
    dtQuat    = dmpPar.dtQuat;
    sQuat     = currClock.quat;
    
    %% Check for user defined extra forces
    if(~isempty(varargin)>0)
        extraForce = varargin{1};
        if(length(varargin)>1)
            extraForceInt = varargin{2};
        end
    else
        extraForce.pos  = [0; 0; 0];
        extraForce.quat = [0; 0; 0];
        extraForceInt.pos  = [0; 0; 0];
        extraForceInt.quat = [0; 0; 0];
    end
    
    %% Update DMP State
    % Position
    sPos          = sPos + (-alphaPos*sPos)*dtPos/tauPos;
    nextClock.pos = sPos;
    tmZero = goalPos - dtPos*dataLength*goalVel;
    tm = tmZero - goalVel*log(sPos)/(dmpPar.tauPos*dmpPar.alphaPos);
    posErr = tm - currState.pos;
    posDis = 0*(goalPos - initPos)*sPos;
    linAcc = (kPos*((1-sPos)*posErr - posDis) + dPos*(1-sPos)*(goalVel - currState.linVel) + extraForce.pos)/tauPos;
    % Compute nonlinear forcing term
    if(isfield(dmpPar,'posForceW'))
        if(~isempty(dmpPar.posForceW))
            f = dmpNonlinearForce(dmpPar.posForceW, dmpPar.posCenter, dmpPar.posAmp, dmpPar.kPos, sPos);
            linAcc = linAcc + f;
        end
    end
    nextState.linAcc = linAcc;
    % Update linear velocity
    nextState.linVel = currState.linVel + linAcc*dtPos + extraForceInt.pos;
    % Update position
    nextState.pos = currState.pos + nextState.linVel*dtPos; 
    
    nextState.tmPos = tm;

    % Orientation
    sQuat          = sQuat + (-alphaQuat*sQuat)*dtQuat/tauQuat;
    nextClock.quat = sQuat;
    %tmZeroQuat = goalQuat - dtPos*dataLength*goalQuatVel;
    tmZeroQuat = quatIntegral(goalQuat, -goalangVel, dtPos*dataLength);
    tm = quatIntegral(tmZeroQuat, -goalangVel, log(sQuat)/(dmpPar.tauQuat*dmpPar.alphaQuat));
   % tm = quatNormalize(tm);
    quatErr = quatError(tm, currState.quat, dmpPar.errMethod);
    quatDis = 0*quatError(goalQuat, initQuat, dmpPar.errMethod)*sQuat;
    angAcc  = (kQuat*((1 - sQuat)*quatErr - quatDis) + dQuat*(1 - sQuat)*(goalangVel - currState.angVel) + extraForce.quat)/tauQuat;
    % Compute nonlinear forcing term
    if(isfield(dmpPar,'quatForceW'))
        if(~isempty(dmpPar.quatForceW))
            f = dmpNonlinearForce(dmpPar.quatForceW, dmpPar.quatCenter, dmpPar.quatAmp, dmpPar.kQuat, sQuat);
            angAcc = angAcc + f;
        end
    end
    nextState.angAcc = angAcc;
    % Update angular velocity
    nextState.angVel = currState.angVel + angAcc*dtQuat + extraForceInt.quat;
    % Update orientation
    nextState.quat = (quatIntegral(currState.quat, nextState.angVel, dtQuat));
    
    nextState.tmQuat = tm;
end