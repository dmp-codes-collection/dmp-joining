function dmpState = simulatePoseDMPblending2(dmpPar, plotPose, maxIter)
    if(isempty(maxIter))
        maxIter = 1000;
    end
    
    % Initial DMP state
    currState.pos = dmpPar.initPos;
    currState.linVel = dmpPar.initVel;
    currState.quat = dmpPar.initQuat;
    currState.angVel = dmpPar.initangVel;
    currClock.pos  = 1;
    currClock.quat = 1;
    dmpState = currState;
    % Loop variables
    itNum    = 1;
    posErrNorm  = norm(dmpPar.goalPos - currState.pos);    
    quatErrNorm = norm(quatError(dmpPar.goalQuat, currState.quat, dmpPar.errMethod));
    dmpState.linAcc = [0;0;0];
    dmpState.angAcc = [0;0;0];
    while((posErrNorm>1e-3 || quatErrNorm>1e-3) && itNum<maxIter)
        % Update DMP state
        [currState, currClock] = computeNextStatePoseDMPblending2(dmpPar, currState, currClock);
        
        itNum = itNum + 1;
        
        % Store DMP state
        dmpState.pos(:,itNum) = currState.pos;
        dmpState.linVel(:,itNum) = currState.linVel;
        dmpState.linAcc(:,itNum) = currState.linAcc;
        dmpState.quat(:,itNum) = currState.quat;
        dmpState.angVel(:,itNum) = currState.angVel;
        dmpState.angAcc(:,itNum) = currState.angAcc;
        
        dmpState.tmPos(:,itNum-1) = currState.tmPos;
        dmpState.tmQuat(:,itNum-1) = currState.tmQuat;

        % Recalculate distance to the goal
        posErrNorm  = norm(dmpPar.goalPos - currState.pos);
        quatErrNorm = norm(quatError(dmpPar.goalQuat, currState.quat, dmpPar.errMethod));
    end
    
    % Plot results
    if(plotPose)
        currPos = dmpState.pos;
        dtPos = dmpPar.dtPos;
        figure('NumberTitle', 'off', 'Name', 'Position trajectory'); 
        plot(0:dtPos:dtPos*(size(currPos,2)-1), currPos, 'LineWidth', 2);
        hold on;
        plot(0:dtPos:dtPos*(size(currPos,2)-1), repmat(dmpPar.goalPos,1,size(currPos,2)))
        
        currQuat = dmpState.quat;
        dtQuat = dmpPar.dtQuat;
        figure('NumberTitle', 'off', 'Name', 'Orientation trajectory'); 
        plot(0:dtQuat:dtPos*(size(currQuat,2)-1), currQuat, 'LineWidth', 2);
        hold on;
        plot(0:dtQuat:dtPos*(size(currQuat,2)-1), repmat(dmpPar.goalQuat,1,size(currQuat,2)))
    end
end