function [nextState, nextClock] = computeNextStatePoseDMP(dmpPar, currState, currClock, varargin)
    %% Get DMP parameters
    % Position DMP parameters
    alphaPos = dmpPar.alphaPos;
    tauPos   = dmpPar.tauPos;
    kPos     = dmpPar.kPos;
    dPos     = dmpPar.dPos;
    goalPos  = dmpPar.goalPos;
    initPos  = dmpPar.initPos;
    dtPos    = dmpPar.dtPos;
    sPos     = currClock.pos;
    
    % Orientation DMP parameters
    alphaQuat = dmpPar.alphaQuat;
    tauQuat   = dmpPar.tauQuat;
    kQuat     = dmpPar.kQuat;
    dQuat     = dmpPar.dQuat;
    goalQuat  = dmpPar.goalQuat;
    initQuat  = dmpPar.initQuat;
    dtQuat    = dmpPar.dtQuat;
    sQuat     = currClock.quat;
    
    %% Check for user defined extra forces
    if(~isempty(varargin)>0)
        extraForce = varargin{1};
        if(length(varargin)>1)
            extraForceInt = varargin{2};
        end
    else
        extraForce.pos  = [0; 0; 0];
        extraForce.quat = [0; 0; 0];
        extraForceInt.pos  = [0; 0; 0];
        extraForceInt.quat = [0; 0; 0];
    end
    
    %% Update DMP State
    % Position
    sPos          = sPos + (-alphaPos*sPos)*dtPos/tauPos;
    nextClock.pos = sPos;
    
    posErr = goalPos - currState.pos;
    posDis = (goalPos - initPos)*sPos;
    linAcc = (kPos*(posErr - posDis) - dPos*currState.linVel + extraForce.pos)/tauPos;
    % Compute nonlinear forcing term
    if(isfield(dmpPar,'posForceW'))
        if(~isempty(dmpPar.posForceW))
            f = dmpNonlinearForce(dmpPar.posForceW, dmpPar.posCenter, dmpPar.posAmp, dmpPar.kPos, sPos);
            linAcc = linAcc + f;
        end
    end
    nextState.linAcc = linAcc;
    % Update linear velocity
    nextState.linVel = currState.linVel + linAcc*dtPos + extraForceInt.pos;
    % Update position
    nextState.pos = currState.pos + nextState.linVel*dtPos; 

    % Orientation
    sQuat          = sQuat + (-alphaQuat*sQuat)*dtQuat/tauQuat;
    nextClock.quat = sQuat;
    
    quatErr = quatError(goalQuat, currState.quat, dmpPar.errMethod);
    quatDis = quatError(goalQuat, initQuat, dmpPar.errMethod)*sQuat;
    angAcc  = (kQuat*(quatErr - quatDis) - dQuat*currState.angVel + extraForce.quat)/tauQuat;
    % Compute nonlinear forcing term
    if(isfield(dmpPar,'quatForceW'))
        if(~isempty(dmpPar.quatForceW))
            f = dmpNonlinearForce(dmpPar.quatForceW, dmpPar.quatCenter, dmpPar.quatAmp, dmpPar.kQuat, sQuat);
            angAcc = angAcc + f;
        end
    end
    nextState.angAcc = angAcc;
    % Update angular velocity
    nextState.angVel = currState.angVel + angAcc*dtQuat + extraForceInt.quat;
    % Update orientation
    nextState.quat = quatNormalize(quatIntegral(currState.quat, nextState.angVel, dtQuat));
end