%%
% Implementation of the "third approach" described in
% Saveriano et al., Merging Position and Orientation Motion Primitives,
% ICRA, 2019 (Section III.C)
%%
clear;
close all; 

addpath('myQuaternion')
%addpath('poseDMP')

% Position DMP parameters
dmpPar.alphaPos = 1;
dmpPar.tauPos   = 1;
dmpPar.sigmaPos = 0.02;
dmpPar.kPos     = 10;
dmpPar.dPos     = 2*sqrt(dmpPar.kPos);
dmpPar.dtPos    = 0.01;
dmpPar.nCompPos = 30;

% Orientation DMP parameters
dmpPar.alphaQuat = 1;
dmpPar.tauQuat   = 1;
dmpPar.sigmaQuat = 0.02;
dmpPar.kQuat     = 10;
dmpPar.dQuat     = 2*sqrt(dmpPar.kQuat*dmpPar.tauQuat);
dmpPar.dtQuat    = 0.01;
dmpPar.errMethod = 'vector';
dmpPar.nCompQuat = 15;

%% Prepare training data using minimum jerk trajectory
% Goal and initial position
dmpPar1 = dmpPar;
dmpPar1.nbData = 501;
% Goal and initial position
dmpPar1.goalPos = [0; 1; -0.3];
dmpPar1.initPos = [1.5; -1; 0.8];

% Goal and initial quaterion
dmpPar1.goalQuat = quatNormalize([0.2471; 0.1797; 0.3182; -0.8974]);
dmpPar1.initQuat = quatNormalize([0.3717; -0.4993; -0.6162; 0.4825]);

initState.pos  = dmpPar1.initPos;
initState.linVel = [0;0;0];
initState.linAcc = [0;0;0];

initState.quat = dmpPar1.initQuat;
initState.angVel = [0;0;0];
initState.angAcc = [0;0;0];

goalState = initState;
goalState.pos  = dmpPar1.goalPos;
goalState.quat = dmpPar1.goalQuat;

T = 5; % Time duration

% Generate position data with minimum jerk trajectory
trainData1 = minJerkTrajectory(initState, goalState, T, dmpPar.dtPos);

% Goal and initial position
dmpPar2 = dmpPar;
dmpPar2.nbData = 501;
% DMP 2
dmpPar2.goalPos = dmpPar1.initPos;
dmpPar2.initPos = [0; 1; -0.3]; %dmpPar1.goalPos;

% Goal and initial quaterion
dmpPar2.goalQuat = dmpPar1.initQuat;
dmpPar2.initQuat = dmpPar1.goalQuat;

initState.pos  = dmpPar2.initPos;
initState.linVel = [0;0.1;0];
initState.linAcc = [0;0;0];

initState.quat = dmpPar2.initQuat;
initState.angVel = [0;0;0];
initState.angAcc = [0;0;0];

goalState = initState;
goalState.pos  = dmpPar2.goalPos;
goalState.quat = dmpPar2.goalQuat;

% Generate position data with minimum jerk trajectory
trainData2 = minJerkTrajectory(initState, goalState, T, dmpPar.dtPos);

%% Train the DMPs separately
dmpPar1 = trainThirdApproachDMP(trainData1, dmpPar1);
dmpPar2 = trainThirdApproachDMP(trainData2, dmpPar2);

totaldmpPar{1} = dmpPar1;
totaldmpPar{2} = dmpPar2;

%% Reproduce joined trajectory
dmpState = simulateThirdApproachDMP(totaldmpPar, 0, 10000);

%% Calculate reproduction error
for i=1:size(dmpState.pos, 2)
    if(i<=dmpPar1.nbData)
        pErr(i) = norm(dmpState.pos(:,i)-trainData1.pos(:,i));
        qErr(i) = norm(quatError(dmpState.quat(:,i), trainData1.quat(:,i), 'logarithm'));
    elseif(i<=dmpPar1.nbData+dmpPar2.nbData)
        pErr(i) = norm(dmpState.pos(:,i)-trainData2.pos(:,i-dmpPar1.nbData));
        qErr(i) = norm(quatError(dmpState.quat(:,i), trainData2.quat(:,i-dmpPar1.nbData), 'logarithm'));
    else
        pErr(i) = norm(dmpState.pos(:,i)-trainData2.pos(:,end));
        qErr(i) = norm(quatError(dmpState.quat(:,i), trainData2.quat(:,end), 'logarithm'));
    end
end

%% Plots
trainData.pos = [trainData1.pos, trainData2.pos];
trainData.quat = [trainData1.quat, trainData2.quat];
trainData.linVel = [trainData1.linVel, trainData2.linVel];
trainData.angVel = [trainData1.angVel, trainData2.angVel];

figure
subplot(2,4,1)
dtPos = dmpPar.dtQuat;
plot(0:dtPos:dtPos*(size(dmpState.quat,2)-1), dmpState.pos, 'LineWidth', 2);
hold on
plot(0:dtPos:dtPos*(size(trainData.pos,2)-1), trainData.pos, 'k');
title('Position')

subplot(2,4,2)
plot(0:dtPos:dtPos*(size(dmpState.linVel,2)-1), dmpState.linVel, 'LineWidth', 2);
hold on
plot(0:dtPos:dtPos*(size(trainData.linVel,2)-1), trainData.linVel, 'k');
title('Linear velocity')

subplot(2,4,5)
dtPos = dmpPar.dtQuat;
plot(0:dtPos:dtPos*(size(dmpState.quat,2)-1), dmpState.quat, 'LineWidth', 2);
hold on
plot(0:dtPos:dtPos*(size(trainData.quat,2)-1), trainData.quat, 'k');
title('Orientation')

subplot(2,4,6)
plot(0:dtPos:dtPos*(size(dmpState.angVel,2)-1), dmpState.angVel, 'LineWidth', 2);
hold on
plot(0:dtPos:dtPos*(size(trainData.angVel,2)-1), trainData.angVel, 'k');
title('Angular velocity')

% Errors
subplot(2,4,3)
plot(0:dtPos:dtPos*(length(pErr)-1),pErr,'k')
title('Position error')

subplot(2,4,7)
plot(0:dtPos:dtPos*(length(qErr)-1),qErr,'k')
title('Orientation error')

subplot(2,4,4)
plot(0:dtPos:dtPos*(size(dmpState.goalFunc,2)-1), dmpState.goalFunc, 'LineWidth', 2);
axis([0,12,-1.1,1.6])
title('Moving target pos')

subplot(2,4,8)
dtPos = dmpPar1.dtPos;
plot(0:dtPos:dtPos*(size(dmpState.goalQuatFunc,2)-1), dmpState.goalQuatFunc, 'LineWidth', 2);
axis([0,12,-1,0.8])
title('Moving target quat')
